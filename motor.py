#!/usr/bin/python3
"""motor.py
Rick Eason
March 2024
"""

import time
import threading
import board

from adafruit_motor import stepper
from adafruit_motorkit import MotorKit

from threading import Lock

plock = Lock()
mlock = Lock()

# If motor address is different, run "i2cdetect -y 1" from the command line to find it
motor_I2C_address = 0x64

class Periodic:
    """This class can call a specified function at specified interval a specified number of times
    This creates another thread that will run the function
    Any data accessed by this thread and another thread must be "lock"ed when reading/writing
    """
    def __init__(self, fct):
        self.fct = fct
        self.thread = threading.Timer(0,lambda:None)        # Dummy thread, already dead

    def is_stopped(self):
        with plock:
            return not self.thread.is_alive()

    def restart(self, interval, num_times):
        """This will abort any previous thread and continue on with new parameters"""
        with plock:
            self.next_t = time.time()
            self.num_times = num_times
            self.interval = interval
            self.thread.cancel()            # Cancel any preexisting thread
        self._run()

    def _run(self):
#        print("hello ", self.num_times)
        self.fct()
        with plock:
            self.num_times -= 1
            if self.num_times > 0:
                self.next_t += self.interval
                self.thread = threading.Timer(self.next_t - time.time(), self._run)
                self.thread.start()
    
    def stop(self):
        with plock:
            self.num_times = 0


class Motor:
    """Basic Motor class, all units are in motor steps
    """
    # stepper options: 'SINGLE' 'DOUBLE' (full step) 
    #                  'INTERLEAVE' (half step) 
    #                  'MICROSTEP' (smallest configured step)
    def __init__(self, style = stepper.SINGLE):
        global motor_I2C_address

        self.motor = MotorKit(i2c=board.I2C(), address=motor_I2C_address)
        self.style = style
        self.dir = stepper.FORWARD          # opposite is stepper.BACKWARD
        self._setposition(0)
        self.setvelocity(10)               # default velocity
        self.service = Periodic(self.step)

    def _setposition(self, pos):
        with mlock:
            self.position = pos

    def getposition(self):
        """Get the given position with safe access to data"""
        with mlock:
            return self.position

    def setvelocity(self, vel):
        """Set the position with safe access to data"""
        with mlock:
            self.velocity = abs(vel)

    def home(self, wait_prompt=False): 
        '''Turn the motor off and set the position to 0, redefining this as "home"'''
        self.off()
        if wait_prompt:
            input("Hit enter when motor is home")
        self._setposition(0)

    def step(self):         # This function is called by another thread, requiring locks
        with mlock:
            self.motor.stepper1.onestep(direction=self.dir, style=self.style)
            self.position += 1 if self.dir == stepper.FORWARD else -1

    def slew(self, dist):         # Just race to do given # steps -- for testing only
        with mlock:
            direction = stepper.FORWARD if dist>=0 else stepper.BACKWARD
            for _ in range(abs(dist)):
                self.motor.stepper1.onestep(direction=self.dir, style=self.style)
                self.position += 1 if self.dir == stepper.FORWARD else -1

    def move(self, position, velocity=None):     # steps and steps_per_second
        """Move to the given stepper position at the given velocity (steps/second)"""
        dist = position - self.getposition()
        self.moveRel(dist, velocity)

    def moveRel(self, distance, velocity=None):      # steps and steps_per_second
        """Move to the given stepper distance at the given velocity (steps/second)"""
#        print(f'Moving {distance=}, {velocity=}')
        with mlock:
            if velocity == None:
                velocity = self.velocity        # Use default
            else:
                self.velocity = abs(velocity)        # save this for next time
            self.dir = stepper.FORWARD if distance>=0 else stepper.BACKWARD
        if distance != 0:
            self.service.restart(1./velocity, distance if distance >= 0 else -distance)

    def stop(self):
        """Stops the motor. Does not turn it off"""
        self.service.stop()

    def is_stopped(self):
        """Returns True iff the motor is currently stopped"""
        return self.service.is_stopped()

    def waitstop(self):
        """Waits for the motor to stop. Blocks execution of this thread"""
        while not self.service.is_stopped():
            time.sleep(0.1)

    def off(self):
        """Disengage the motor so it moves freely"""
        self.service.stop()
        self.motor.stepper1.release()


class MLmotor(Motor):
    """Version of the motor class which interacts in units of ml (for syringe pump)"""
    def __init__(self, stepsPerML):
        self.spml = stepsPerML
        Motor.__init__(self)

    def moveML(self, ml, secpml):
        """Move the motor to given ml position. Velocity is in seconds per ml"""
        Motor.move(self, ml*self.spml, self.spml/secpml)

    def moveRelML(self, ml, secpml):
        """Move the motor a relative number of ml. Velocity is in seconds per ml"""
        Motor.moveRel(self, ml*self.spml, self.spml/secpml)

    def getpositionML(self):
        """Get the position in units of ml"""
        return self.getposition() / self.spml


def testStuff():
#    m = Motor()
#    m.home()
#    m.move(1000,10)
#    time.sleep(3)
#    m.moveRel(-10,10)
    m = MLmotor(870)
    m.moveRel(1,5)
#    time.sleep(3)
#    m.move(10,10)
#    time.sleep(3)
#    m.move(-10,10)
#    time.sleep(3)

if __name__ == "__main__":

    testStuff()

