
ERS 602 Icecore project notes
Developed by Rick Easton
March 2024
Fresh Raspberry Pi OS (64-bit) install
Do updates
$ sudo apt update;sudo apt uprade -y

$ sudo raspi-config
    Interface Options: Enable SSH and I2C

$ mkdir icecore
$ cd icecore

$ python -m venv env --system-site-packages
$ source env/bin/activate

$ pip3 install adafruit-circuitpython-motorkit

# See if I2C address of motor is still 64
$ i2cdetect -y 1        # may require: $ sudo apt install i2c-tools

$ vi ~/.bashrc
    changed 32m to 35m
$ source ~/.bashrc

$ ssh-keygen
$ vi ~/.ssh/authorized_keys
    copy remote machine's id_rsa.pub contents into here as an entry

$ sudo vi /etc/ssh/sshd_config                # Disable Password logins
    Change entry to: PasswordAuthentication no

$ sudo apt update; sudo apt install ufw
$ sudo ufw default deny incoming
$ sudo ufw allow from 192.168.1.0/24 to any port 22		# use this one
$ sudo ufw enable
$ sudo ufw status

$ sudo apt install vim
# Add the following ~/.vimrc file (in order to allow ctr-shift-c copy from selection):
****
unlet! skip_defaults_vim
source $VIMRUNTIME/defaults.vim

:set mouse=v
****

*************************************************
NOTE: When logging in:
*************************************************
$ cd icecore
$ source env/bin/activate

Run using:
$ ./syringe config_20ml.txt
or
$ python3 syringe.py config_20ml.txt
*************************************************


# NOTE ALSO:
    $ sudo ufw status numbered
    $ sudo ufw delete 1
    $ sudo ufw reload
    $ sudo ufw enable

    $ sudo ufw allow 22						# Allow port 22 to everyone in the world
    $ sudo ufw allow from 192.168.1.0/24	# Allow all ports on my local network
    $ sudo ufw allow 80						# Allow web ports to everyone
    $ sudo ufw --force enable


*********************************************************************************************************
Threading:
https://realpython.com/intro-to-python-threading/

https://medium.com/analytics-vidhya/how-to-create-a-thread-safe-singleton-class-in-python-822e1170a7f6
https://www.geeksforgeeks.org/singleton-method-python-design-patterns/
https://www.tutorialspoint.com/python_design_patterns/python_design_patterns_singleton.htm
https://stackoverflow.com/questions/12435211/threading-timer-repeat-function-every-n-seconds

