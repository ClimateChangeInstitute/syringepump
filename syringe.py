#!/home/lab/icecore/env/bin/python3
"""syringe.py
Rick Eason
March 2024
"""

import sys
from datetime import datetime
import atexit
import tkinter as tk
from tkinter import ttk
from tkinter.filedialog import askopenfile, asksaveasfile

from motor import MLmotor

# Possibly issue: When Pi is not connected to the network, 
#    it loses track of time when turned off, so logging has incorrect datetime
#    possible fix is to ask for date and time on startup.

m = MLmotor(1)      # Dummy placeholder version

# States and state changes allowed:
# Home (turn off motor)
#     Loading (button press)
#     Homing (button press/menu)
#     Calibrating (menu)
# Loading
#     Loaded (m.is_stopped(), position = target)
# Loaded
#     Loading (button press)
#     Unloading (button press)
#     Homing (button press/menu)
#     Calibrating (menu)
# Unloading
#     Home (m.is_stopped() && position == 0
# Homing -- Dialog box
#     Home (when done)
# Calibrating -- Dialog box
#     Loaded (when done)
#
# Stop button pauses the motor, but doesn't change state


def donothing():
    pass
     
class Mainwindow(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.title("Syringe Pump Controller")
#        self.top.geometry("450x300")
        self.option_add( "*font", "Arial 16" )
#        self.top.resizable(False, False)

        self.bind('<Control-q>', lambda e: self.quit())

        self.frm = MyFrame(self)
#        self.frm.config(bg="#AAAAFF")
        self.do_menu()
        
    def do_menu(self):
        """Menu for the main window"""
        menubar = tk.Menu(self)
        filemenu = tk.Menu(menubar, tearoff=False)
#        filemenu.add_command(label="Open", underline=0, command=self.file_open)
#        filemenu.add_command(label="Save", underline=0, command=self.file_save)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", underline=1, command=self.quit, accelerator="Ctrl+Q")
        menubar.add_cascade(label="File", underline=0, menu=filemenu)

        toolsmenu = tk.Menu(menubar, tearoff=False)
        toolsmenu.add_command(label="Home syringe", underline=0, command=self.frm.home)
        toolsmenu.add_command(label="Calibrate", underline=0, command=self.frm.calibrate)
        menubar.add_cascade(label="Tools", underline=0, menu=toolsmenu)

        helpmenu = tk.Menu(menubar, tearoff=False)
#        helpmenu.add_command(label="Help Index", underline=0, command=donothing)
        helpmenu.add_command(label="About...", underline=0, command=self.about_box)
        menubar.add_cascade(label="Help", underline=0, menu=helpmenu)

        self.config(menu=menubar)

# file_open() and file_save() work, but menu for them might be currently disabled
    def file_open(self):
        infile = askopenfile(mode ='r', filetypes =[('Configuration Files', '*.txt')])
        if infile != None:
            lines = infile.read()
            vals = lines.strip().split(":")
            if len(vals) == 2 and vals[0].strip() =='Steps/ml':
                spml = int(float(vals[1]) + .5)         # +.5 for rounding
            infile.close()

    def file_save(self):
        outfile = asksaveasfile(mode ='w', filetypes =[('Configuration Files', '*.txt')])
        if outfile != None:
            text2save = f'Steps/ml: {m.spml}\n'
            outfile.write(text2save)
            outfile.close()

    def about_box(self):
        tk.messagebox.showinfo("About","Syringe Pump Controller\n\nVersion 1.0\nRick Eason\n2024-3-9")


class MyFrame(tk.Frame):
    """Frame for the main window"""
    def __init__(self, parent):
        global configfile     # Only to pass the name to the logger -- dumb

        tk.Frame.__init__(self, parent)

        self.position = 0     # current position, updated periodically from motor
        self.fill_increment = 1     # How much to load on each "Load Syringe!"
        self.num_fills = 0          # Number of times we've hit "Load Syringe!"
        self.load_velocity = 1      # const, sec/ml set above what is possible
        self.unload_velocity = self.last_unload_velocity = 60
        self.status = "Home"       # See top of code for states and state changes 
        self.logger = Logger()
        self.logger.log(f'Starting Syringe Pump Operation\n{"*"*52}')
        self.logger.log(f'Config file: {configfile}')

        self.grid()
        self.display()
        
    def display(self):
        """Layout the frame of the main window"""
        global m

        # Column 0 labels
        for i,label in enumerate(('Load Increment (ml)', '', '', 'Syringe Status', 'Syringe Contents (ml)', 'Unload Rate (sec/ml)')):
            tk.Label(self, text=label+'  ').grid(column=0, row=i, sticky='e')

        # Variables and their callbacks
        self.ent0_val = tk.StringVar(value=1)
        self.uvel_val = tk.StringVar(value=60)
        self.ent0_val.trace_add('write', self.ent0_callback)
        self.uvel_val.trace_add('write', self.uvel_callback)

        # Loading boxes and labels
        self.entry0 = tk.Entry(self, textvariable = self.ent0_val, width=10)
        self.entry0.grid(column=1, row=0)

        self.count_label = tk.Label(self, text='# loads:  0', width=11, bg='#AAFFAA')
        self.count_label.grid(column=2, row=1)

        # Separator
        ttk.Style().configure('blue.TSeparator', background='blue')
        separator = ttk.Separator(self, orient=tk.HORIZONTAL, style='blue.TSeparator')
        separator.grid(row=2,column=0,columnspan=3, ipadx=100, sticky="ew")

        # Unloading boxes and labels
        self.status_label = tk.Label(self, text='Home', bg="#80FFFF", width=12)
        self.status_label.grid(column=1, row=3)

        self.act_remaining_label = tk.Label(self, text='1:00 sec remain', bg="#80FFFF", width=13)
        self.act_remaining_label.grid(column=2, row=3)

        self.pos_boxML = tk.Label(self, text='0', width = 12, bg="#80FFFF")
        self.pos_boxML.grid(column=1, row=4)

        self.uvel_entry = tk.Entry(self, textvariable = self.uvel_val, width=12)
        self.uvel_entry.grid(column=1, row=5)

        self.uvel_label= tk.Label(self, text=f'1.00 min / ml', width = 12, bg="#DDDDDD")
        self.uvel_label.grid(column=1, row=6)

        self.remaining_label = tk.Label(self, text='1:00 sec remain')
        self.remaining_label.grid(column=2, row=6)

#        # Blank line
#        tk.Label(self, text='').grid(column=0, row=6)

        #Buttons
#        quit_button = tk.Button(self, text = "Quit", command=self.top.destroy).grid(column=3, row=0)
        load_button = tk.Button(self, text = "Load Syringe!", command=self.load).grid(column=2,row=0)
        self.unload_button = tk.Button(self, text = "Unload Syringe!", command=self.unload, width=13)
        self.unload_button.grid(column=2,row=5)
        home_button = tk.Button(self, text = "Home", command=self.home).grid(column=0,row=7,sticky='w')
        stop_button = tk.Button(self, text = "Stop", command=self.stop).grid(column=2,row=7,sticky='e')

        self.update_position()          # This starts the update process

    def load(self):     # incremental load
        """Callback to initiate the syringe loading"""
        global m
        if self.status == "Home" or self.status=="Loaded":
            m.moveRelML(self.fill_increment, self.load_velocity)            # Use relative steps 
            self.change_status("Loading")
            self.change_num_fills(self.num_fills+1)          # We assume the load will complete
            self.logger.log(f'Loading {self.fill_increment:.2f} ml: #{self.num_fills}')

    def unload(self):
        """Callback to initiate the syringe unloading"""
        global m
        if self.status == "Loaded" or self.status == "Unloading":   # Allow speed changes while unloading
            m.moveML(0, self.unload_velocity)
            self.change_status("Unloading")
            self.last_unload_velocity = self.unload_velocity
            self.logger.log(f'Unloading {self.position/m.spml:.2f} ml @ {self.unload_velocity} sec/ml')

    def stop(self):
        """Button callback to stop motor and pause movement. No statechanges"""
        global m
        m.stop()
        m.off()
        self.logger.log(f'Stopped at {self.position/m.spml:.2f} ml')

    def home(self):
        """Button callback to open a modal dialog to home the motor. 
        It is modal, so no further GUI actions happen until the dialog finishes
        """
        global m
        if self.status == "Home" or self.status=="Loaded":
            self.change_status("Homing")
            self.logger.log(f'Begin homing')
            HomeDialog(self).show()
            m.home()                    # Resets position to 0
            self.change_status("Home")

    def calibrate(self):
        """Button callback to open a modal dialog to calibrate the motor. 
        It is modal, so no further GUI actions happen until the dialog finishes
        """
        global m
        if self.status == "Home" or self.status=="Loaded":
            self.change_status("Calibrating")
            self.logger.log(f'Begin calibration with {m.getposition()} steps')
            CalibrateDialog(self).show()
            self.logger.log(f'End calibration with {m.getposition()} steps')
            self.change_status("Loaded")

    def change_status(self, status):
        """Do status changes through this function to ensure stuff gets done"""
        self.status = status
        self.status_label.config(text=self.status)
        if status == "Home":
            self.change_num_fills(0)
            m.off()                         # Turn motor off
            self.logger.log(f'Home')

        if status == "Unloading":
            self.unload_button.config(text = "Change rate")
        else:
            self.unload_button.config(text = "Unload Syringe!")

    def change_num_fills(self, num_fills):
        """Changes the num_fills value and update displayed value"""
        self.num_fills = num_fills
        self.count_label.config(text=f'# loads:  {self.num_fills}')

    def update_position(self):
        """This function repeatedly calls itself to update the displayed values"""
        self.position = m.getposition()
        self.pos_boxML.config(text = f'{self.position/m.spml:.2f}')
        remain = int(self.position / m.spml * self.unload_velocity + .5)
        self.remaining_label.config(text = f'{remain//60:2d}:{remain%60:02d} remain')
        if self.status == "Unloading":          # Reset to last used 
            remain = int(self.position / m.spml * self.last_unload_velocity + .5)
        self.act_remaining_label.config(text = f'{remain//60:2d}:{remain%60:02d} remain')
        if (self.status == "Unloading" or self.status == "Loading") and m.is_stopped():
            self.change_status("Loaded" if self.position > 0 else "Home")

        self.after(100, self.update_position)  # run itself again after 100 ms

    def ent0_callback(self, var, index, mode):
        """Callback to update display window and record valid values of desired fill increment"""
        try:
#            if self.entry0.focus_get() is self.entry0:
            val = float(self.ent0_val.get())
            if val <= 0:
                raise
            self.fill_increment = val
            self.entry0.config(bg="white")
        except:
            self.entry0.config(bg="#FFDDDD")

    def uvel_callback(self, var, index, mode):
        """Callback to update display window and record valid values of desired unload rate"""
        try:
#            if self.uvel_entry.focus_get() is self.uvel_entry:
            val = float(self.uvel_val.get())
            if val <= 0:
                raise
            self.unload_velocity = val
            self.uvel_label.config(text = str(f'{val/60:.2f} min / ml'))
            self.uvel_entry.config(bg="white")
        except:
            self.uvel_entry.config(bg="#FFDDDD")


    def on_click(self):
        """Test function -- not used"""
        HomeDialog(self).show()
#        result = TestDialog(self).show()
#        self.label.configure(text="your result: '%s'" % result)


class ModalDialog:
    """Basic modal dialog that blocks until it is exited """
    def __init__(self, parent):
        self.toplevel = tk.Toplevel(parent)

    def show(self):
        self.toplevel.grab_set()
        self.toplevel.wait_window()
        self.toplevel = None


class HomeDialog(ModalDialog):
    """Modal dialog box for homing the motor (sending it to it's unloaded position).
    On exit, the motor's position is set to zero, which indicates the syringe is unloaded.
    It is modal, so no further GUI actions happen until the dialog finishes
    """
    def __init__(self, parent):
        ModalDialog.__init__(self, parent)
        self.toplevel.title("Find Motor Home")
#        self.toplevel.config(bg="#4444FF")
        instructions = """
Use "Move" button to move a bit at a time. 

If the motor hits the end (and maybe 
stops turning), then hit "Stop". 
Note that when the motor is stopped,
you can move it by hand. 

Hit "Done" when finished.
        """
        label = tk.Label(self.toplevel, text=instructions, justify=tk.LEFT, padx=30)
        move_button = tk.Button(self.toplevel, text="Move a bit", command=self.moveabit)
        stop_button = tk.Button(self.toplevel, text="Stop", command=self.stop)
        done_button = tk.Button(self.toplevel, text="Done", command=self.toplevel.destroy)
            
        label.pack(side="top", fill=tk.BOTH)
        move_button.pack(side="left", fill=tk.BOTH)
        done_button.pack(side="right", fill=tk.BOTH)
        stop_button.pack(side="bottom", fill=tk.BOTH)
#        done_button.pack(side="bottom", anchor="e", padx=4, pady=4)

    def moveabit(self):     # Our own copy which moves in steps rather than ML
        """Inidate a move of 1000 steps towards home at the maximum rate"""
        global m
        m.moveRel(-1000, 1000)            # Move a bit

    def stop(self):
        """Stop and turn off motor without logging"""
        global m
        m.stop()
        m.home()


class CalibrateDialog(ModalDialog):
    """Modal dialog box for calibrating the motor (determining the steps per ml)
    It is modal, so no further GUI actions happen until the dialog finishes
    """
    def __init__(self, parent):
        ModalDialog.__init__(self, parent)

        self.toplevel.title("Calibration")
        instructions = """
Make sure motor was homed before loading started.
Move the motor to a desired "number of steps". 
Stop and/or Move as desired, then hit "Done"
Consider "Unloading" the syringe when finished. 
        """
        self.target_val = tk.StringVar(value=1000)
        self.target_val.trace_add('write', self.target_callback)

        instructions_label = tk.Label(self.toplevel, text=instructions, justify=tk.LEFT, padx=30)
        self.position_label = tk.Label(self.toplevel, text="0", padx=30, width = 10)
        self.target_entry = tk.Entry(self.toplevel, textvariable = self.target_val, justify=tk.CENTER, width=10)

        move_button = tk.Button(self.toplevel, text="Move", command=self.movetotarget)
        stop_button = tk.Button(self.toplevel, text="Stop", command=m.stop)
        done_button = tk.Button(self.toplevel, text="Done", command=self.toplevel.destroy)
            
        instructions_label.pack(side="top", fill=tk.BOTH)
        self.position_label.pack(side="top", fill=tk.BOTH)
        self.target_entry.pack(side="top")
        move_button.pack(side="left", fill=tk.BOTH)
        done_button.pack(side="right", fill=tk.BOTH)
        stop_button.pack(side="bottom", fill=tk.BOTH)
        self.update_position()                         # This starts the update process

    def movetotarget(self):     # Move 1000 steps
        """Move towards the given target position at the maximum rate"""
        global m
        try:
            target = float(self.target_val.get())
            m.move(target, 1000)            # Move a bit
        except:
            pass

    def target_callback(self, var, index, mode):
        """Callback to update display window and record valid values of desired target position"""
        try:
            val = float(self.target_val.get())
            if val <= 0:
                raise
            self.target_entry.config(bg="white")
        except:
            self.target_entry.config(bg="#FFDDDD")

    def update_position(self):
        """This function repeatedly calls itself to update the displayed position"""
        position = m.getposition()
        self.position_label.config(text = f'Current position: {position:.0f}')
        self.toplevel.after(100, self.update_position)  # run itself again after 100 ms


class TestDialog(ModalDialog):
    """Test modal dialog box showing how to pass back a message"""
    def __init__(self, parent, prompt):
        ModalDialog.__init__(self, parent)
        self.var = tk.StringVar()
        label = tk.Label(self.toplevel, text=prompt)
        entry = tk.Entry(self.toplevel, width=40, textvariable=self.var)
        button = tk.Button(self.toplevel, text="OK", command=self.toplevel.destroy)
            
        label.pack(side="top", fill=tk.BOTH)
        entry.pack(side="top", fill=tk.BOTH)
        button.pack(side="bottom", anchor="e", padx=4, pady=4)

    def show(self):
        ModalDialog.show(self)

        value = self.var.get()
        return value
    

class Logger:
    """Class for logging (appending) the given info to the log file with datetime stamp."""
    def __init__(self):
        self.filename = f'log_{datetime.now().strftime("%Y_%m_%d")}.log'
                
        atexit.register(self.exit_handler)

    def log(self, string):
        """Append the given string to the log file with datetime stamp"""
        with open(self.filename, 'a') as outfile:
            outfile.write(f'{datetime.now().strftime("%Y/%m/%d %H:%M:%S")}: {string}\n')

    def exit_handler(self):
        """Automatically called on exit to log exit time to the log file"""
        self.log(f'Exiting Syringe Pump operation\n{"*"*52}')


def create_motor(configfile):
    """Reads config file in a fairly robust way. 
    The file must contain two lines with "steps: <number>" and "ml: <number>"
    Other lines are ignored, colons are optional
    """
    global m

    try:
        with open(configfile, 'r') as cfile:
            lines = cfile.readlines()
    except:
        print(f'Error: could not open file: "{configfile}"')
        exit()

    try:
        kvs = dict()
        for line in lines:
            items = line.replace(':', ' ').lower().split()
            if len(items)==2:
                k, v = items
                kvs[k] = float(v)
        spml = kvs['steps']/kvs['ml']
    except:
        print('Error reading config file. Make sure it contains lines containing\n',
            'steps: <number>\n',
            'ml: <number>\n',
        )
        exit()
    m = MLmotor(spml)


if __name__ == "__main__":
    global configfile       # only to pass the name to the Logger
    
    if len(sys.argv) > 2:
        print(f'Usage {sys.argv[0]} [filename]')
        exit()
    if len(sys.argv) == 2:
        configfile = sys.argv[1]
    else:
        configfile = input("Enter name of config file: ")

    create_motor(configfile)

    # Now for the GUI stuff
    root = Mainwindow()
    root.mainloop()







