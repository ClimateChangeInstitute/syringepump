# syringepump
## Authors and acknowledgment
Rick Eason, UMaine

## Hardware 
```
 Wijnen B, Hunt EJ, Anzalone GC, Pearce JM (2014) Open-Source Syringe Pump Library. PLoS ONE 9(9): e107216. https://doi.org/10.1371/journal.pone.0107216
```

Updated design https://www.thingiverse.com/thing:819723

* Stepper motor - NEMA-17 size - 200 steps/rev, 12V 350mA
* Tutorials: http://www.adafruit.com/products/324#tutorials
* Aluminum Flex Shaft Coupler - 5mm to 5mm
* Tutorials: http://www.adafruit.com/products/1175#tutorials
* Adafruit  DC & Stepper Motor HAT for Raspberry Pi - Mini Kit
* Tutorials: https://www.adafruit.com/product/2348


## git lab getting started

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ClimateChangeInstitute/syringepump.git
git branch -M main
git push -uf origin main
```

## Installation
Follow instruction in SetupNotes.txt


